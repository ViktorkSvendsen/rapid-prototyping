Prototyping Report

Project overview

Event Title:    Nordic Game Jam    Date: 17.04.20 - 19.04.20
Event Type:    Gamejam    Location: Denmark
Author:    Viktor Kind Svendsen

Project Title:
Butterfly madness

Project Group:    Role    Expertise
Viktor Kind Svendsen    Programmer    Beginner
    

Technology
Development Environment:
Unity hub
Unity

Target Platform:
Web based on desktop machines

Code download/install instructions:
https://itch.io/jam/oohgamejam/rate/639892


Description

When the theme for the jam “comfort” was announced, as well as the administrators pointed out to lay out a focus on learning, i got started. This time, i just wanted to finish a platformer, and rather build upon it as much as i could, so i knew whatever i ended up with, it would be considered a “playable” game. 

The concept of the game was to make a game where u would atleast learn one thing. I looked into how to implement textboxes which came up during the game. These textboxes would ask the player something, and give the player 2 alternatives. In my head there would be like a question “what is 2+2?”. After the question, 2 “pipes” like the one from Super Mario would show up, and the player could jump into one of them. One of them had “4” on them (the right answer), and the other could suggest like “8”(the wrong answer).





Contribution 

The areas of the game that I worked on were: the main menu, the butterfly controller, the staminabar, (some of the graphics but we never implemented them), sugarwater(stat boost, refills stamina + temp boost).


Things that worked well: Communication and having fun together while learning. 


Things that went badly or slower than expected: Alot of things went wrong in this project, since we had very little experience in unity. I came across my group in the discord for the game jam. Skitz talked and started to organize early like he had the most knowledge when we formed a group, but turned out he had never written a line of code before in any language, just watched a couple of youtube videos. It was kinda fun when we found out. 

Im not gonna put the blame on anyone, and it was a mistake, but the biggest problem was on the last day. Will made a backup of some sort, and i dont know what he pushed and what he didnt push, but everything stopped working. There were copies of stuff being referenced twice, and whatnot. Nothing worked. We already had bad enough time, and this set us back alot. We didnt really get to do much the last 6 hours, as it took will like 4 hours to fix it. We made a couple of things but we never got to implement it. It was also alot of stuff that ACTUALLY worked and was in the game, but Will removed it. Like it was kinda demotivating to see most of the game just destroyed, as we had little time left, our goal was just to make it compile. I tried to explain that we could go back on an earlier committ and work from there, but it was too much chaos, and Will had never worked on a project with other people before, and was not familiar with merge conflicts, pushing or pulling and whatnot. Will fixed this by doing all this work manually, by copying old files and pasting them into new ones, he also renamed every single thing i think while doing this. Or almost. 

I personally wasted a lot of time on 2 things.
 
The first was to make visual studio actually synch with the project and autocomplete my sentences, the online suggestions did not work, even though, among the many, the right one was actually there. The right solution had very few upvotes compared to the other solutions so it took a while before i gave it a shot.  

The second problem i had was just make the “play button actually work, funny enough, cause it was one of the simplest things i worked with. My problem was the fact that even though the button was synched up with an object, it would not detect the functions in that object. I sat with it for a while, and followed a youtube tutorial like 5 times, but still did not manage it. I cant even remember how i fixed it, i googled alot and alot of bad answers came up, which i all tried. Cant remember how it worked, but i think i had an Eureka moment. Just tried something and it worked. 


Reflection

The Good:
The group.The main reason the game jam was so fun for me i think was the collaboration and planning with the rest of the group. We shared the screen when somebody got an error or some bug occured, and it was sometimes pretty hilarous. Also fun that we were all from different parts of the world, put together with one goal in mind. 
Good communication.  One of the advantages of being put in a room with other people all focused on the same project is that you do not need to spend the time discussing what happened last night, or the issues of the world.  
Awesome group - The group was pretty cool because we were all fairly new. The group consisted of Linda, from the Netherlands, Will from the states, Me from Norway and Skitz from Denmark. We had alot of fun, and nobody had a bad tone or were negative in any way. 

The Bad:
Lack of sleep - this is a common problems with intensive work periods describe what you felt were the consequences of lack of sleep rather than just say that it happened
Bugs introduced in the last hour - The merge problems and how the code broke the last day screwed us over. As this was my first unity project, it was hard to coordinate how to solve this problem. I knew what had to be done, but Will said he would fix it. He kinda did but also messed up the game alot, turning it into another game then what we actually had working. If something like this happens in the future, i will know how to adress it. I will also ask specifically how someone plans to “solve it” if they are saying they will handle the situation.
Unable to integrate the artists work - We made some stuff, in different scenes, but didnt manage to actually get it into the main game due to an error. 
Final game was not fun. It didnt work properly.



The things that I would do differently are:

Better communication - If errors occurs, especially gamebreaking ones, these needs to be handled properly and with supervision. In our case, one of the developers had never really fixed a problem of this size before, and im not sure if he knew any other alternatives. When working with people online, i will make sure to communicate better.



Learning reflection:

I really learned alot about game development and unity this session. I have made many applications and whatnot, but i have never really worked with graphics in any way. So using my programming skills to make stuff happen with a physics engine was really really cool. What amazes me the most is how much i actually enjoyed this. I have never really made a game, only logic for it honestly, and i never thought making a game could be this fun.
I think the problems we met this project could be avoided if i took more action. As i think i was the most experienced programmer, even though not in unity, i should have said something. 

Conclusion:
This gamejam opened up a new world for me, i can definitely see myself making more games as a hobby now, i really enjoyed it. 
