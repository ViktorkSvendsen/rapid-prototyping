﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerUp : MonoBehaviour
{
    public GameObject pickupEffect;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            Pickup();
        }
    }

    void Pickup() {

        Destroy(gameObject);
    }
}
